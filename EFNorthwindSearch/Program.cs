﻿using EFNorthwindSearch.Models;

namespace EFNorthwindSearch
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DisplayProductCategories();
            GetProductByCategory();
        }

        static void DisplayProductCategories()
        {
            var context = new NorthwindContext();
            var allProductCategoriesQuery = from c in context.Categories
                                   select c;

            Console.WriteLine("Product Categories are:");
            foreach (Category c in allProductCategoriesQuery)
            {
                Console.WriteLine(
                $"ID:{c.CategoryId} - {c.CategoryName}");
            }

            
        }

            static void GetProductByCategory() {

            Console.WriteLine("Enter Product Category Name:");
            string productCatgName = Console.ReadLine();

            var context = new NorthwindContext();
            var productByCatgQuery =
                from p in context.Products
                where p.Category.CategoryName == productCatgName
                orderby p.CategoryId, p.ProductName
                select new
                {
                    p.ProductId,
                    p.ProductName,
                    p.UnitPrice,
                    Category = p.Category.CategoryName,
                };

            foreach (var product in productByCatgQuery)
            {
                Console.WriteLine(
                    $"(#{product.ProductId} {product.ProductName} - Category: {product.Category} Price: {product.UnitPrice})");
            }

        }
    }
}